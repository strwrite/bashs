#!/bin/bash

mkdir -p ~/.bashdone_str.d

function ibdone  {
	if [[ -f ~/.bashdone_str.d/$1 ]] ; then
		return 0
	else
		return 1
	fi
}

function bdone {
	touch ~/.bashdone_str.d/$1
}

echo "Doing autoapply"

echo "Already applied:"
ls ~/.bashdone_str.d


if  ! ibdone complete-alias  ; then 
	echo "Applying alias autocompletion"
	bdone complete-alias
	curl -L https://raw.githubusercontent.com/cykerway/complete-alias/master/bash_completion.sh >> ~/.bash_completion

	if hash kubectl 2>/dev/null ; then
		echo 'complete -F _complete_alias kl' >> ~/.bash_completion
	fi	

	if hash docker-compose 2>/dev/null ; then 
		echo 'complete -F _complete_alias dc' >> ~/.bash_completion
	fi
fi

if ! ibdone kl-alias ; then
	if hash kubectl 2>/dev/null ; then 
		echo "Applying alias for kubectl: kl"
		bdone kl-alias
		echo 'source <(kubectl completion bash)' >> ~/.bashrc
		echo 'alias kl=kubectl' >> ~/.bashrc
	fi
fi

if ! ibdone dc-alias ; then 
	if hash docker-compose 2>/dev/null ; then
		echo "Applying alias for docker-compose: dc" 
		bdone dc-alias
		echo 'alias dc=docker-compose' >> ~/.bashrc
	fi
fi

echo "Done"